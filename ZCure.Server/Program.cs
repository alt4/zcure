﻿using ZCure.API.Servers;
using System.Net;
using ZCure.API;
using ZCure.API.Configuration;
using CommandLine;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;
using System.Threading.Tasks.Sources;
using System.Runtime.Loader;

namespace ZCure.Server
{

    public class CliOptions
    {
        [Option('c', "config", Default = "zcure.yml", HelpText = "path to configuration file")]
        public string ConfigFile { get; set; } = "zcure.yml";
    }

    internal class Program
    {
        static void StartApp(CliOptions opts)
        {
            AppConfig config = new AppConfig();

            if(File.Exists(opts.ConfigFile))
            {
                var yamlSerializer = new DeserializerBuilder()
                    .WithNamingConvention(CamelCaseNamingConvention.Instance)
                    .Build();

                config = yamlSerializer.Deserialize<AppConfig>(File.ReadAllText(opts.ConfigFile));
            }

            // parse config from environment
            config.Database.Database = Environment.GetEnvironmentVariable("ZCURE_DB_DATABASE") ?? config.Database.Database;
            config.Database.Host = Environment.GetEnvironmentVariable("ZCURE_DB_HOST") ?? config.Database.Host;
            string? port = Environment.GetEnvironmentVariable("ZCURE_DB_PORT");
            config.Database.Port = port != null ? int.Parse(port) : config.Database.Port;
            config.Database.User = Environment.GetEnvironmentVariable("ZCURE_DB_USER") ?? config.Database.User;
            config.Database.Password = Environment.GetEnvironmentVariable("ZCURE_DB_PASSWORD") ?? config.Database.Password;
            config.Steam.ApiKey = Environment.GetEnvironmentVariable("ZCURE_STEAM_API_KEY") ?? config.Steam.ApiKey;

            ZCureApp app = new ZCureApp(config);
            app.Initialize();

            HttpServer httpServer = new HttpServer(app, IPAddress.Parse(app.Configuration.WebServer.Ip), app.Configuration.WebServer.Port, "HTTP", "INT");
            httpServer.Start();

            var presence = new TcpServer(app, IPAddress.Parse(app.Configuration.PresenceServer.Ip), app.Configuration.PresenceServer.Port);
            presence.Start();

            var endEvent = new ManualResetEventSlim();
            var startEnd = new ManualResetEventSlim();

            AssemblyLoadContext.Default.Unloading += (ctx) =>
            {
                startEnd.Set();
                endEvent.Wait();
            };

            startEnd.Wait();
            httpServer.Stop();
            presence.Stop();
            endEvent.Set();
        }

        static void Main(string[] args) =>
            CommandLine.Parser.Default
                .ParseArguments<CliOptions>(args)
                .WithParsed(StartApp);
    }
}