# ZCure Master Server

The main server for Blacklight: Retribution v3.02 completely rewritten in .NET8.
Based on the decompiled [ZCAPI](https://gitlab.com/blrevive/reverse/blrcrashreporter/-/tree/1.12-Arc?ref_type=heads) library from the `BLRCrashReporter.exe` of an older BL:R build.

## Setup

### Docker (recommended)

* download [docker/zcure.yml]
* setup database
  * use [docker/zcure.db.yml] for a preconfigured mysql container
  * see manual steps for instructions how to setup your own instance

### Manual steps

* download or build [ZCure.Server]
* import [docker/zcure.sql] to your database
* configure [ZCure.Server] to use your database

## Usage

* start [ZCure.Server]
* launch BL:R client: `$ FoxGame-win32-Shipping.exe -zcureurl=127.0.0.1 -zcureport=8080 -presenceurl=127.0.0.1 -presenceport=9004` (make sure to adjust urls and ports)

[docker/zcure.yml]: ./docker/zcure.yml
[docker/zcure.db.yml]: ./docker/zcure.db.yml
[docker/zcure.sql]: ./docker/zcure.sql
[ZCure.Server]: ./ZCure.Server/Readme.md
[ZCure.Server/Config]: ./ZCure.Server/Documentation/Config.md
