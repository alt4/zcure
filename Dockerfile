ARG BUILD_ENV=local
# Doesn't matter when BUILD_ENV is "ci"
ARG BUILD_TYPE=Debug
ARG RUNTIME_TAG=8.0-alpine3.19-amd64

# If BUILD_ENV is set to "ci", assume the program is already built in the "bin" directory and ready to be consumed
FROM scratch as build_ci
ONBUILD COPY bin/* /opt/zcure

# If BUILD_ENV is set to "local", assume binaries need to be built during the image preparation process
FROM mcr.microsoft.com/dotnet/sdk:8.0 as build_local
ONBUILD ARG BUILD_TYPE
ONBUILD COPY ZCure.API /tmp/zcure/ZCure.API
ONBUILD COPY ZCure.Server /tmp/zcure/ZCure.Server
ONBUILD COPY ZCure.sln /tmp/zcure/ZCure.sln
ONBUILD WORKDIR /tmp/zcure/
ONBUILD RUN dotnet publish -c ${BUILD_TYPE} -r linux-musl-x64 -o /opt/zcure

# Dummy stage to converge between both methods
FROM build_${BUILD_ENV} as build

FROM mcr.microsoft.com/dotnet/runtime:${RUNTIME_TAG} as final

COPY --from=build /opt/zcure /opt/zcure

RUN adduser -Dh /home/zcure zcure
USER zcure

ENV PATH="${PATH}:/opt/zcure"
WORKDIR /home/zcure

ENTRYPOINT ["ZCure.Server"]