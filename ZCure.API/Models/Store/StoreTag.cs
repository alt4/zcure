using System.Xml.Serialization;

namespace ZCure.API.Models.Store
{
    public class StoreTag
    {
        [XmlAttribute("TN")]
        public string Tag { get; set; } = "";

        [XmlElement("ST")]
        public List<StoreTag> ChildTags { get; set; } = new List<StoreTag>();
    }
}