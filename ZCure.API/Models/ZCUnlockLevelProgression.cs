﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ZCure.API.Models
{
    [XmlRoot(ElementName = "LEVELS")]
    public class ZCUnlockLevelProgression
    {
        [XmlElement("ITEM")]
        public UnlockLevel[] UnlockLevels { get; set; }
    }
}
