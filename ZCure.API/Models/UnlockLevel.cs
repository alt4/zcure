﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ZCure.API.Models
{
    [Table("Levels")]
    public class UnlockLevel
    {
        [XmlAttribute("LVL")]
        public int Id { get; set; }

        [XmlAttribute("XP")]
        public int RequiredXP { get; set; }
    }
}
