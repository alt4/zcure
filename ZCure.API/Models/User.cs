﻿using Org.BouncyCastle.Pqc.Crypto.Lms;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZCure.API.Models
{
    public enum EGender
    {
        Male,
        Female
    }

    [Flags]
    public enum EProfileFlag : int
    {
        None = 0,
        CharacterCreated = 1 << 0,
        IsMale = 1 << 1,
        IsFemale = 1 << 2,
        IsDeveloper = 1 << 3,
        IsPress = 1 << 4,
        TrainingComplete = 1 << 5,
        AccountMigrated = 1 << 6,
        FirstPurchase1 = 1 << 7,
        FirstPurchase2 = 1 << 8,
        FirstPurchase3 = 1 << 9,
        FirstPurchase4 = 1 << 10,
        FirstPurchase5 = 1 << 11,
        FirstPurchase6 = 1 << 12
    }

    public enum EProfileTitle : int
    {

    }

    public enum EProfileBadge : int
    {

    }

    [Table("Users")]
    public class User
    {
        [Key]
        public long Id { get; set; }
        public string PassHash { get; set; } = "";

        public string Name { get; set; }

        public DateTime CreatedAt { get; set; } = DateTime.Now;
        public DateTime LastSeenAt { get; set; } = DateTime.Now;

        public int Experience { get; set; }
        public EGender Gender { get; set; }

        public EProfileFlag ProfileFlags { get; set; }

        public EProfileTitle Title { get; set; }
        public EProfileBadge Badge { get; set; }

        public int PrestigeLevel { get; set; }

        public int GP { get; set; }
        public int ZP { get; set; }


        public virtual UserStats Stats { get; set; }

        public byte[]? Settings { get; set; }

        public long SteamId { get; set; }

        public override string ToString()
            => $"User(Name={Name}; ID = {Id})";

        [NotMapped]
        public string ResponseString =>
            $"{Id}:{Name}:{Experience}:{(Gender == EGender.Female ? 'T' : 'F')}:{(int)ProfileFlags}:{(int)Badge}:{Title}:{LastSeenAt}:{PrestigeLevel}";
    }
}
