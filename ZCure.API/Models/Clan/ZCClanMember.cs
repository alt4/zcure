using System.Xml.Serialization;

namespace ZCure.API.Models.Clan
{
    public class ZCClanMember
    {
        [XmlAttribute("MID")]
        public long MemberId { get; set; }

        [XmlAttribute("IID")]
        public long InviterId { get; set; }

        [XmlAttribute("JD")]
        public DateTime JoinedAt { get; set; }

        [XmlAttribute("CID")]
        public Guid ClanId { get; set; }

        [XmlAttribute("RK")]
        public EClanRank Rank { get; set; }
    }
}