using System.Xml.Serialization;

namespace ZCure.API.Models.Clan
{
    public class ZCClan
    {
        [XmlAttribute("CID")]
        public Guid ClanId { get; set; }

        [XmlAttribute("CN")]
        public string Name { get; set; }
        
        [XmlAttribute("CT")]
        public string Tag {get; set;}

        [XmlAttribute("OID")]
        public long OwnerId { get; set; }

        [XmlAttribute("MOTD")]
        public string MOTD { get; set; }

        [XmlAttribute("DF")]
        public DateTime FoundedAt { get; set; }

        [XmlAttribute("ML")]
        public ushort MemberLimit { get; set; }
    }
}