using System.Xml.Serialization;

namespace ZCure.API.Models.Clan
{
    [XmlRoot("CCI")]
    public struct ZCClanCombinedInfo
    {
        [XmlElement("CI")]
        public ZCClan Clan {get; set;}

        [XmlElement("ML")]
        public List<ZCClanMemberCombinedInfo> Members {get; set;}

        public List<ZCClanInviteInfo> Invites { get; set; }
    }
}