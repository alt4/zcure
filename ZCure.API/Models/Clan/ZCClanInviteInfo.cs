using System.Xml.Serialization;

namespace ZCure.API.Models.Clan
{
    public class ZCClanInviteInfo
    {
        [XmlElement("CN")]
        public ZCClan Clan { get; set;}

        [XmlAttribute("MC")]
        public ushort MemberCount { get; set; }

        [XmlAttribute("IN")]
        public long InviterId { get; set; }
    }
}