namespace ZCure.API.Models.Clan
{
    public enum EClanRank
    {
        Leader,
        Officer,
        Member,
        Recruit,
        Invited
    }
}