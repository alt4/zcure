using System.Xml.Serialization;

namespace ZCure.API.Models
{
    [XmlRoot("ZCMission")]
    public class ZCMission
    {
        [XmlElement("MI")]
        public List<MissionItem> Items {get; set;}

        [XmlElement("OID")]
        public long OwnerId {get; set;}
    }
}