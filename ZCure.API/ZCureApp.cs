using Dapper;
using MySql.Data.MySqlClient;
using Serilog;
using Serilog.Configuration;
using System.Reflection;
using System.Text;
using System.Xml.Linq;
using ZCure.API.Caches;
using ZCure.API.Configuration;
using ZCure.API.Database;
using ZCure.API.Requests;
using ZCure.API.Services;

namespace ZCure.API
{
    public class ZCureApp
    {
        public bool IsInitialized { get; protected set; } = false;

        public Repositories Database { get; protected set; }

        public AppConfig Configuration { get; protected set; }

        private ILogger _log;

        private Dictionary<Type, ICache> _caches = new Dictionary<Type, ICache>();

        private Dictionary<Type, IService> _services = new Dictionary<Type, IService>();

        public ZCureApp(AppConfig configuration)
        {
            Configuration = configuration;
            Log.Logger = new ZCure.API.Logger(Configuration.Logging.GetLoggerConfig().CreateLogger());
            _log = Log.ForContext<ZCureApp>();
        }

        public void Initialize()
        {
            _log.Debug("initializing zcure app");

            _log.Debug("check database connection to {Provider}", Configuration.Database.Provider);
            if (!CheckDatabaseConnection())
                System.Environment.Exit(1);
            ConditionalMigrateTables();

            Database = new Repositories(this);
            SimpleCRUD.SetDialect(SimpleCRUD.Dialect.MySQL);
            SqlMapper.AddTypeHandler(new MySqlGuidTypeHandler());
            SqlMapper.RemoveTypeMap(typeof(Guid));
            SqlMapper.RemoveTypeMap(typeof(Guid?));

            _log.Debug("initializing caches");
            AddCache(new FileCache());
            AddCache(new StoreCache(Database));
            AddCache(new AuthCache());
            AddCache(new ConnectionCache());

            _log.Debug("initializing services");
            AddService<UserService>();
            AddService<PresenceService>();
            AddService<AuthService>();
            AddService<InventoryService>();
            AddService<GameServerService>();
            AddService<FriendService>();
            InitializeServices();

            _log.Information("zcure app initialized");
            IsInitialized = true;
        }

        public bool AddCache<T>(T cache) where T : ICache
        {
            if (_caches.ContainsKey(typeof(T)))
                return false;

            _caches.Add(typeof(T), cache);
            return true;
        }

        public T? GetCache<T>() where T : ICache
        {
            return (T?)_caches.GetValueOrDefault(typeof(T));
        }

        public bool RemoveCache<T>() where T : ICache
        {
            if (!_caches.ContainsKey(typeof(T)))
                return false;

            _caches.Remove(typeof(T));
            return true;
        }

        public bool AddService<T>() where T : IService
            => AddService(Activator.CreateInstance<T>());

        public bool AddService<T>(T service) where T : IService
        {
            if (_services.ContainsKey(typeof(T)))
                return false;

            _services.Add(typeof(T), service);
            return true;
        }

        public T? GetService<T>() where T : IService
            => (T?)_services.GetValueOrDefault(typeof(T));

        public bool RemoveService<T>() where T : IService
        {
            if (!_services.ContainsKey(typeof(T)))
                return false;

            _services.Remove(typeof(T));
            return true;
        }

        public void InitializeServices()
        {
            foreach (IService service in _services.Values)
                service.Initialize(this);
        }

        public bool CheckDatabaseConnection()
        {
            try
            {
                using (var conn = new MySqlConnection(Configuration.Database.ConnectionString()))
                {
                    conn.Open();
                    conn.Close();
                }
            }
            catch(Exception ex)
            {
                _log.Error(ex, "failed to connect to database");
                return false;
            }

            return true;
        }

        public void ConditionalMigrateTables()
        {
            var tables = GetMigrationFileList().Select(mf => mf.Split("_").ElementAt(1));

            using(var conn = new MySqlConnection(Configuration.Database.ConnectionString()))
            {
                foreach(var tableName in tables)
                {
                    if (DoesTableExist(conn, tableName))
                        continue;

                    _log.Debug("running migration for table {TableName}", tableName);
                    string migration = ReadResource(tableName + ".sql");
                    int count = conn.Execute(migration);
                    _log.Debug("migration succeeded with {Count} rows affected", count);
                }
            }
        }

        private bool DoesTableExist(MySqlConnection conn, string tableName)
        {
            int c = conn.ExecuteScalar<int>("select case when exists((select * from information_schema.tables where table_name = @TableName and table_schema = @TableSchema)) then 1 else 0 end", new
            {
                TableName = tableName,
                TableSchema = Configuration.Database.Database
            });
            return c == 1;
        }

        public List<string> GetMigrationFileList()
        {
            // Determine path
            var assembly = Assembly.GetExecutingAssembly();
            // Format: "{Namespace}.{Folder}.{filename}.{Extension}"
            var resourcePath = assembly.GetManifestResourceNames()
                    .Where(str => str.EndsWith(".sql"))
                    .Select(str => str.Split(".").Reverse().ElementAt(1));
            return resourcePath.ToList();
        }

        public string ReadResource(string name)
        {
            // Determine path
            var assembly = Assembly.GetExecutingAssembly();
            string resourcePath = name;
            // Format: "{Namespace}.{Folder}.{filename}.{Extension}"
            if (!name.StartsWith(nameof(ZCure)))
            {
                resourcePath = assembly.GetManifestResourceNames()
                    .Single(str => str.EndsWith(name));
            }

            using (Stream stream = assembly.GetManifestResourceStream(resourcePath))
            using (StreamReader reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }
    }
}