﻿using Dapper;
using MySql.Data.MySqlClient;
using Serilog;
using Serilog.Core;
using System;
using System.Collections.Generic;
using System.Data;

namespace ZCure.API.Database
{
    public class MySqlGuidTypeHandler : SqlMapper.TypeHandler<Guid>
    {
        public override void SetValue(IDbDataParameter parameter, Guid guid)
        {
            parameter.Value = guid.ToString();
        }

        public override Guid Parse(object value)
        {
            return new Guid((string)value);
        }
    }

    public class Repository<TEntity, TKey>
    {
        protected ZCureApp _app;
        protected ILogger _log;
        
        public Repository(ZCureApp app) 
        { 
            _app = app;
            _log = Log.ForContext<Repository<TEntity, TKey>>();
        }

        public TEntity? Get(TKey id)
            => Execute(c => c.Get<TEntity>(id));

        public IEnumerable<TEntity> GetList()
            => Execute(c => c.GetList<TEntity>())!;

        public IEnumerable<TEntity> Where(object conditions)
            => Execute(c => c.GetList<TEntity>(conditions))!;

        public IEnumerable<TEntity> Where(string conditions, object? param = null)
            => Execute(c => c.GetList<TEntity>(conditions, param))!;

        public TKey? Insert(TEntity entity)
            => Execute(c => c.Insert<TKey, TEntity>(entity));

        public int Update(TEntity entity)
            => Execute(c => c.Update(entity));

        public int Delete(TEntity entity)
            => Execute(c => c.Delete(entity));

        public int Delete(TKey id)
            => Execute(c => c.Delete<TEntity>(id));

        public int Delete(object conditions)
            => Execute(c => c.DeleteList<TEntity>(conditions));

        protected IDbConnection GetConnection()
        {
            switch(_app.Configuration.Database.Provider.ToLower())
            {
                case "mysql":
                    return new MySqlConnection(_app.Configuration.Database.ConnectionString());
            }

            throw new ArgumentException($"database provider {_app.Configuration.Database.Provider} is not supported");
        }

        protected TRet? Execute<TRet>(Func<IDbConnection, TRet?> func)
        {
            TRet? result = default(TRet);

            try
            {
                using (var conn = GetConnection())
                {
                    conn.Open();
                    result = func(conn);
                    conn.Close();
                }
            } 
            catch(Exception ex)
            {
                _log.Error(ex, "failed to execute database transaction");
            }

            return result;
        }
    }
}
