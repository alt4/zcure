CREATE TABLE IF NOT EXISTS `Loadouts` (
  `Id` varchar(36) NOT NULL,
  `ParentId` varchar(36) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `OwnerId` bigint NOT NULL,
  `CreatorId` bigint NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `ModifiedAt` datetime NOT NULL,
  `Data` text NOT NULL,
  `Installed` int NOT NULL,
  `ShareCode` varchar(36) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;