﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZCure.API.Caches;
using ZCure.API.Models;

namespace ZCure.API.Services
{
    public class InventoryService : Service
    {
        private StoreCache _storeCache => _app.GetCache<StoreCache>()!;

        private UserService _userService => _app.GetService<UserService>()!;

        private long _inventoryTemplateId = -1;

        public override void Initialize(ZCureApp app)
        {
            base.Initialize(app);

            _inventoryTemplateId = _app.Configuration.Inventory.TemplateId;
            //_userService.OnUserCreated += InitializeUserInventory;
        }
    }
}
