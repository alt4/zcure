﻿using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using ZCure.API.Caches;
using ZCure.API.Connections;
using ZCure.API.Controllers;
using ZCure.API.Models;

namespace ZCure.API.Services
{
    public class UserService : Service
    {
        public delegate void UserLoggedInDelegate(User user, ISession session);
        public delegate void UserLoggedOutDelegate(User user, ISession session);

        public delegate void CreateUserDelegate(ref User user);
        public delegate void UserCreatedDelegate(User user);

        public delegate void UpdateUserDelegate(ref User user);
        public delegate void UserUpdatedDelegate(User user);

        public delegate void DeleteUserDelegate(ref User user);
        public delegate void UserDeletedDelegate(User user);

        public UserLoggedInDelegate OnUserLoggedIn;
        public UserLoggedOutDelegate OnUserLoggedOut;

        public CreateUserDelegate OnCreateUser;
        public UserCreatedDelegate OnUserCreated;

        public UpdateUserDelegate OnUpdateUser;
        public UserUpdatedDelegate OnUserUpdated;

        public DeleteUserDelegate OnDeleteUser;
        public UserDeletedDelegate OnUserDeleted;

        private ConnectionCache _connections => _app.GetCache<ConnectionCache>()!;

        private AuthService _authService => _app.GetService<AuthService>()!;
        private AuthCache _authCache => _app.GetCache<AuthCache>()!;


        public User CreateUser(string username, long steamId)
        {
            User user = new User()
            {
                Name = username,
                SteamId = steamId,
                CreatedAt = DateTime.Now,
                LastSeenAt = DateTime.Now,
                PassHash = "",
                ProfileFlags = 0
            };

            if(OnCreateUser != null)
                OnCreateUser(ref user);

            user.Id = _database.Users.Insert(user);

            if(OnUserCreated != null)
                OnUserCreated(user);

            return user;
        }

        public (ELoginResponse, EConnectionStatus, AuthTicket?) SteamLogin(
            string username, long steamId, string steamTicket, ISession session)
        {
            _log.Debug("begin steam login for {UserName} ({SteamId}) on {IP}", username, steamId, (session.Socket.RemoteEndPoint as IPEndPoint)!.Address);

            if (!_app.Configuration.Steam.DisableSteamAuth && !_authService.ValidateSteamWebTicket(steamTicket, steamId))
                return (ELoginResponse.Failed, EConnectionStatus.InvalidUser, null);

            User? user = _database.Users.Where(new { SteamId = steamId }).FirstOrDefault();

            if (user == null)
            {
                _log.Debug("user not found, creating new user for {SteamId}", steamId);
                user = CreateUser(username, steamId);

                if(user == null)
                {
                    _log.Error("failed to create new user for initial login");
                    return (ELoginResponse.Failed, EConnectionStatus.UnkownError, null);
                }

                _log.Information("created new user {User} for initial login", user);
            }

            if(OnUserLoggedIn != null)
                OnUserLoggedIn(user, session);
            
            return (ELoginResponse.OK, EConnectionStatus.Connected, _authCache.Get(user.Id));
        }

        public void Logout(User? user, ISession session)
        {
            if (user == null)
                return;

            TcpSession? tcpSession = _connections.Get(user.Id);

            if (tcpSession != null)
            {
                tcpSession.Disconnect();
                _log.Debug("disconnecting tcp session for {User} because of logout", user);
            }

            if(OnUserLoggedOut != null)
                OnUserLoggedOut(user, session);

            _log.Information("user {User} logged out", user);
        }


    }
}
