﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;
using ZCure.API.Models.Game;

namespace ZCure.API.Services
{
    internal class GameServerEntry
    {
        public string ID { get; set; }
        public string ServerAddress { get; set; }
        public int Port { get; set; } = 7777;
        public int InfoPort { get; set; } = 7777;
        public string Region { get; set; }
    }

    internal class BlreServerInfo
    {
        public string GameMode { get; set; } = "";
        public string ServerName { get; set; } = "";
        public string Map { get; set; } = "";

        public int PlayerCount { get; set; } = 0;
        public int MaxPlayers { get; set; } = 16;
        public int BotCount { get; set; } = 0;

        public int GoalScore { get; set; }
        public int TimeLimit { get; set; }

        public GameServerInfo ToGameServerInfo(string address, int gamePort)
        {
            return new GameServerInfo()
            {
                Address = address,
                Port = gamePort,
                ServerName = ServerName,
                CurrentPlayerCount = PlayerCount,
                MaxPlayerCount = MaxPlayers,
                OwnerName = "",
                GoalScore = GoalScore,
                CurrentMap = Enum.Parse<EGameMap>(Map),
                GameMode = Enum.Parse<EGameMode>(GameMode)
            };
        }
    }

    public class GameServerService : Service
    {
        private List<GameServerInfo> _gameServerInfos = new List<GameServerInfo>();
        private List<GameServerEntry> _gameServerList = new List<GameServerEntry>();


        private Timer _updateGameServerListTimer;
        private Timer _updateGameServerInfoTimer;


        public override void Initialize(ZCureApp app)
        {
            base.Initialize(app);

            UpdateGameServerList();

            _updateGameServerListTimer = new Timer((x) => UpdateGameServerList(), null, TimeSpan.FromMinutes(10), TimeSpan.FromHours(1));
            _updateGameServerInfoTimer = new Timer((x) => UpdateGameServerInfos(), null, TimeSpan.FromMinutes(1), TimeSpan.FromMinutes(1));
        }

        public override void Shutdown()
        {
            _updateGameServerInfoTimer.Dispose();
            _updateGameServerListTimer.Dispose();

            base.Shutdown();
        }

        public List<GameServerInfo> GetGameServerInfos()
        {
            lock(_gameServerInfos)
            {
                return _gameServerInfos;
            }
        }

        private async void UpdateGameServerList()
        {
            HttpClient client = new HttpClient();
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "https://raw.githubusercontent.com/HALOMAXX/BLREdit/master/Resources/ServerList.json");
            HttpResponseMessage response = await client.SendAsync(request);

            if(!response.IsSuccessStatusCode)
            {
                _log.Error("failed to retrieve game server list with {StatusCode}", response.StatusCode);
                return;
            }

            List<GameServerEntry>? gameServers = await response.Content.ReadFromJsonAsync<List<GameServerEntry>>();

            if(gameServers == null)
            {
                _log.Error("failed to deserialize game server list");
                return;
            }

            lock(_gameServerList)
            {
                _gameServerList = gameServers;
            }

            _log.Information("updated game server list");
            UpdateGameServerInfos();
        }

        private async void UpdateGameServerInfos()
        {
            IEnumerable<Task<GameServerInfo?>> serverInfoQueries =
                from gameServer in _gameServerList
                select GetGameServerInfo(gameServer);

            List<Task<GameServerInfo?>> serverInfoTasks = serverInfoQueries.ToList();
            _log.Debug("updating game server infos for {Count} servers", _gameServerList.Count);

            while (serverInfoTasks.Any())
            {
                Task<GameServerInfo?> finishedTask = await Task.WhenAny(serverInfoTasks);
                serverInfoTasks.Remove(finishedTask);

                GameServerInfo? info = await finishedTask;

                if (info == null)
                    continue;

                lock(_gameServerInfos)
                {
                    var currGameServerInfo = _gameServerInfos
                        .FirstOrDefault(i => i.Address == info.Address && i.Port == info.Port);

                    if(currGameServerInfo != null)
                    {
                        info.ServerId = currGameServerInfo.ServerId;
                        _gameServerInfos.Remove(currGameServerInfo);
                    }
                    else
                    {
                        info.ServerId = _gameServerInfos.Count();
                    }

                    _log.Debug("updated game server info for {Id} ({Address}:{Port})", info.ServerId, info.Address, info.Port);
                    _gameServerInfos.Add(info);
                }
            }
        }

        private async Task<GameServerInfo?> GetGameServerInfo(GameServerEntry gameServer)
        {
            HttpClient client = new HttpClient();
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, $"http://{gameServer.ServerAddress}:{gameServer.InfoPort}/server_info");

            try
            {
                HttpResponseMessage response = await client.SendAsync(request);

                if (!response.IsSuccessStatusCode)
                {
                    _log.Verbose("failed to get game server info from {Address}:{Port}: {StatusCode}", gameServer.ServerAddress, gameServer.InfoPort, response.StatusCode);
                    return null;
                }

                BlreServerInfo? serverInfo = await response.Content.ReadFromJsonAsync<BlreServerInfo>();
                if (serverInfo == null)
                {
                    _log.Verbose("failed to parse game server info of {GameServerId}", gameServer.ID);
                    return null;
                }

                return serverInfo.ToGameServerInfo(gameServer.ServerAddress, gameServer.Port);
            }
            catch (Exception ex)
            {
                _log.Verbose(ex, "failed to get game server info from {Address}:{Port}", gameServer.ServerAddress, gameServer.Port);
                return null;
            }
        }
    }
}
