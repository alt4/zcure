﻿using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZCure.API.Caches;
using ZCure.API.Connections;
using ZCure.API.Models;
using ZCure.API.Models.Presence;
using ZCure.API.Protocol;

namespace ZCure.API.Services
{
    public enum EPresenceUpdateType
    {
        Add,
        Update,
        Remove
    }

    public class PresenceService : Service
    {
        private ConnectionCache _connectionCache => _app.GetCache<ConnectionCache>()!;

        public PresenceCache PresenceCache { get; protected set; } = new PresenceCache();

        public void ApplyPresence(User user, EPresenceUpdateType updateType, PresenceInfo info)
        {
            bool sendNotification = false;

            _log.Debug("applying user presence for {User}", user);

            switch(updateType)
            {
                case EPresenceUpdateType.Add:
                    sendNotification = AddPresence(user, info);
                    break;
                case EPresenceUpdateType.Update:
                    sendNotification = UpdatePresence(user, info);
                    break;
                case EPresenceUpdateType.Remove:
                    sendNotification = RemovePresence(user, ref info);
                    break;
            }

            if (!sendNotification)
                return;

            SendPresenceUpdate(user, info, updateType);
        }

        public bool AddPresence(User user, PresenceInfo info)
        {
            if (info.UserId != user.Id)
                return false;

            PresenceCache.Add(user.Id, info);
            _log.Debug("added presence info {}", info);
            return true;
        }

        public bool UpdatePresence(User user, PresenceInfo info)
        {
            PresenceInfo? cachedInfo = PresenceCache.Get(user.Id);

            if (cachedInfo == null)
                return AddPresence(user, info);

            _log.Debug("updated presence info {}", info);
            PresenceCache.Update(user.Id, info);

            return true;
        }

        public bool RemovePresence(User user, ref PresenceInfo info)
        {
            PresenceCache.Remove(user.Id);
            info.StatusContext = EOnlineContext.Offline;

            _log.Debug("removed presence info {}", info);
            return true;
        }

        public void SendPresenceUpdate(User user, PresenceInfo info, EPresenceUpdateType updateType)
        {

        }

        public void SendNotification(User user, MessageType messageType, string data)
            => SendNotification(user, messageType, Encoding.UTF8.GetBytes(data));

        public void SendNotification(User user, MessageType messageType, byte[] data)
            => SendNotification(user, new Message(messageType, 0, data));

        public void SendNotification(User user, Message notification)
        {
            TcpSession? connection = _connectionCache.Get(user.Id);

            if (connection == null)
                return;

            connection.SendAsync(notification.Encrypt());
            _log.Debug("sent {MessageType} to {UserId}", notification.MessageType, user.Id);
        }
    }
}
