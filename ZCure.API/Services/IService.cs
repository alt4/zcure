﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZCure.API.Services
{
    public interface IService
    {
        public void Initialize(ZCureApp app);

        public void Shutdown();
    }
}
