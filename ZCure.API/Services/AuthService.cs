﻿using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using ZCure.API.Caches;
using ZCure.API.Connections;
using ZCure.API.Protocol;
using System.Reflection.Metadata.Ecma335;
using ZCure.API.Models;
using Newtonsoft.Json;

namespace ZCure.API.Services
{
    public class AuthService : Service
    {
        private AuthCache _authCache => _app.GetCache<AuthCache>()!;
        private UserService _userService => _app.GetService<UserService>()!;

        public override void Initialize(ZCureApp app)
        {
            base.Initialize(app);

            _userService.OnUserLoggedIn += OnUserLoggedIn;
            _userService.OnUserLoggedOut += OnUserLoggedOut;
        }

        public override void Shutdown()
        {
            if(_userService.OnUserLoggedIn != null)
                _userService.OnUserLoggedIn -= OnUserLoggedIn;
            if(_userService.OnUserLoggedOut != null)
                _userService.OnUserLoggedOut -= OnUserLoggedOut;
        }

        public bool ValidateRequest(IPAddress ip, long userId)
        {
            AuthTicket? ticket = _authCache.Get(userId);

            if(ticket == null)
            {
                _log.Error("no auth ticket available for user {UserId}", userId);
                return false;
            }

            if(!ticket.IP.Equals(ip))
            {
                _log.Error("wrong remote address for auth ticket of user {UserId}: expected {ExpectedIp} but was {ActualIp}", userId, ticket.IP, ip);
                return false;
            }

            if(!ticket.IsValid)
            {
                _log.Error("auth ticket for user {UserId} on {Address} expired", userId, ip);
                return false;
            }

            return true;
        }

        public bool ValidateRequest(ISession session, long userId)
            => ValidateRequest((session.Socket.RemoteEndPoint as IPEndPoint)!.Address, userId);

        public bool ValidateRequest(ISession session, Message query)
        {
            string[] queryData = Encoding.UTF8.GetString(query.Data).Split(":");

            if(queryData.Length == 0)
            {
                _log.Error("failed to validate request {MessageType} on {SessionId}: no message data", query.MessageType, session.Id);
                return false;
            }

            long userId = 0;
            if (!long.TryParse(queryData[0], out userId))
            {
                _log.Error("failed to validate request {MessageType} on {SessionId}: {Data} is not a valid user id", query.MessageType, session.Id, queryData[0]);
                return false;
            }

            return ValidateRequest(session, userId);
        }

        public void OnUserLoggedIn(User user, ISession session)
        {
            IPAddress ip = (session.Socket.RemoteEndPoint as IPEndPoint)!.Address;
            AuthTicket? ticket = _authCache.Get(user.Id);

            if(ticket == null)
                ticket = new AuthTicket(user.Id, ip);

            ticket.IP = ip;
            _authCache.Update(user.Id, ticket);
        }

        public void OnUserLoggedOut(User user, ISession session)
        {
            _authCache.Remove(user.Id);
        }

        public bool ValidateSteamWebTicket(string ticket, long steamId)
        {
            byte[] ticketData = Convert.FromBase64String(ticket);
            string ticketHex = Convert.ToHexString(ticketData);

            string apiUrl = _app.Configuration.Steam.ApiUrl;
            string apiKey = _app.Configuration.Steam.ApiKey;
            int appId = _app.Configuration.Steam.AppId;

            string requestData = $"?key={apiKey}&appid={appId}&ticket={ticketHex}";
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, $"https://{apiUrl}/ISteamUserAuth/AuthenticateUserTicket/v1/{requestData}");

            var httpClient = new System.Net.Http.HttpClient();
            var requestTask = httpClient.SendAsync(request);

            if (!requestTask.Wait(2000))
            {
                _log.Error("timeout while validating steam ticket for {SteamId}", steamId);
                return false;
            }

            HttpResponseMessage response = requestTask.Result;

            if (!response.IsSuccessStatusCode)
            {
                _log.Error("steam api request failed with code: {StatusCode}", response.StatusCode);
                return false;
            }

            var data = JsonConvert.DeserializeAnonymousType(requestTask.Result.Content.ReadAsStringAsync().Result, new
            {
                Response = new
                {
                    Error = new
                    {
                        ErrorCode = 0,
                        ErrorDesc = ""
                    },
                    Params = new
                    {
                        Result = "",
                        SteamId = (long)0,
                        OwnerSteamId = (long)0,
                        VacBanned = false,
                        PublisherBanned = false
                    }
                }
            })!;

            if (data.Response.Error != null)
            {
                _log.Error("failed to validate steam ticket for {SteamId} with code {ErrorCode}: {Description}", steamId, data.Response.Error.ErrorCode, data.Response.Error.ErrorDesc);
                return false;
            }
            else if (data.Response.Params.SteamId != steamId)
            {
                _log.Error("failed to validate steam ticket for {SteamId}: invalid response steam id {ResponseSteamId}", steamId, data.Response.Params.SteamId);
                return false;
            }
            else if (!_app.Configuration.Steam.AllowVacBan && data.Response.Params.VacBanned)
            {
                _log.Error("steam user {SteamId} was banned by VAC", steamId);
                return false;
            }
            else if (!_app.Configuration.Steam.AllowPublisherBan && data.Response.Params.PublisherBanned)
            {
                _log.Error("steam user {SteamId} was banned by publisher", steamId);
                return false;
            }

            _log.Debug("steam ticket validation for {SteamId} was succesfull", steamId);
            return true;
        }
    }
}
