# workflow

<table cellspacing="0" cellpadding="0">
<tr>
<td>

- server receives a request through TCP/UDP connection
- the request is passed to the router
    - splits the request into array of messages (queries)
    - decrypts all messages
    - executes the queries
        - creates instance of controller that owns the query handler
        - deserializes the query message body to array of objects based on query handler arguments
        - calls the query handler with deserialized object array
        - serializes the data returned from query handler
    - encrypts all response messages to construct a response
- server sends response

</td>
<td>

```plantuml
:incoming request;

partition router {
    :split request into messages (queries);

    while(more messages) is (yes)
        :decrypt message;
        :route query to query handler;
        :execute query handler;
        :serialize data;
    endwhile (no)

    :concatenate messages (responses);
}

:send response;
```

</td>
</tr>
</table>

# router

The router takes care of routing query [message]()s to its corresponding [query handler]() and preparing a response which is passed back to the connection handler.

## query mapping

Queries are mapped by annotating the method of a [controller]() with the [QueryAttribute]() which marks the method as [query handler]() for a specific message type. 

Before starting to listen for incoming connections the `ZCure.Api.Router.BuildRoutingMap` method need to be executed which will build a map of [query handler]()s and the message type it should handle.


# controller

A controller groups query handlers which have the same scope of logic (often related to the model(s) used in the execution process, *for example the `UserController` groups all query handlers related to the `User` model*).

The only requirement for a controller class is to inherit from `ZCure.API.Requests.Controller`.

## query handler

A query handler is a method of a controller annotated with the `ZCure.API.Requests.QueryAttribute` which causes the router to route all queries (aka request messages) to this method.

### argument mapping

The arguments of a query handler correspond to the message body data array of the query it handles.

- argument count **must** match the element count of the message body data array
    > if it doesn't the deserialization of the message body data array may file due to the delimiter problem
- argument position **must** match with the index of the message body data array

#### deserialization rules

| condition | deserialization |
|---|---|
| `byte[]` | no deserialization, bytes are copied from the message body buffer |
| `[XmlElement] <Class>` | using `ZCure.API.Serialization.XmlSerializer` to construct `<Class>` from xml string (requires the class to be xml serializable) |
| `string/bool/number/DateTime` | using `Convert.ChangeType` for standard deserliazation from string |


### return type

The query handler is supposed to return data (a response) which is send back to the client.

- query handlers **must** return data so void methods are permitted

The data returned from a query handler is serialized to a response that is passed back to the connection to be send to the client.

| return type | response | meaning | 
|---|---|---|
| `bool` | `T/F` for `true/false` | indicates wether query execution was succesfull |
| `Tuple<bool, ...>` | `T/F:...` | first element of tuple inidicates wether query was succesfull, subsequent elements are serialized using serialization rules below |
| `List<Message>` | message bytes | used for query handlers which return multiple messages at once (for eg `MessageType.GetUserStore`) |

#### serialization rules

Serialization of the returned data to a response follows these rules:

| type | serialization |
|---|---|
| `bool` | `T` for true, `F` for false |
| `string/char` | - |
| `number/DateTime` | using `ToString()` method |
| `byte[]` | no serialization, buffer is copied to message body buffer |
| `[XmlElement] <Class>` | using `ZCure.API.Serialization.XmlSerializer` to serialize `<Class>` to xml string (requires `<Class>` to be xml serializable) |



[QueryAttribute]: ../Requests/QueryAttribute.cs
[controller]: #controller
[query handler]: #query-handler
[message]: ./Protocol.md#message