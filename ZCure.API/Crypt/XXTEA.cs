﻿using System;

namespace ZCure.API.Crypt
{
    public static class XXTEA
    {
        private static uint[] Key = new uint[4]
        {
            4271383519U,
            3752564771U,
            1127922790U,
            1752379389U
        };
        private static int Iterations = 12;

        private static void CodeInternal(ref uint[] v, uint[] k, XXTEAMode mode)
        {
            if (v.Length <= 1)
                return;
            uint y = 0;
            uint z = 0;
            uint sum = 0;
            uint p = 0;
            uint e = 0;
            int length = v.Length;
            Func<uint> func = () => (uint)(((int)(z >> 5) ^ (int)y << 2) + ((int)(y >> 3) ^ (int)z << 4) ^ ((int)sum ^ (int)y) + ((int)k[(nint)(p & 3U ^ e)] ^ (int)z));
            if (mode == XXTEAMode.Encrypt)
            {
                uint num = (uint)(6 + 52 / length);
                z = v[length - 1];
                do
                {
                    sum += 2654435769U;
                    e = sum >> 2 & 3U;
                    for (p = 0U; p < length - 1; ++p)
                    {
                        y = v[(nint)(p + 1U)];
                        z = v[(nint)p] += func();
                    }
                    y = v[0];
                    z = v[length - 1] += func();
                }
                while (--num > 0U);
            }
            else
            {
                sum = (uint)(6 + 52 / length) * 2654435769U;
                y = v[0];
                do
                {
                    e = sum >> 2 & 3U;
                    for (p = (uint)(length - 1); p > 0U; --p)
                    {
                        z = v[(nint)(p - 1U)];
                        y = v[(nint)p] -= func();
                    }
                    z = v[length - 1];
                    y = v[0] -= func();
                }
                while ((sum -= 2654435769U) != 0U);
            }
        }

        public static void DoDecrypt(ref byte[] InOutData)
        {
            uint[] v = new uint[(int)Math.Ceiling(InOutData.Length / 4.0)];
            Buffer.BlockCopy(InOutData, 0, v, 0, InOutData.Length);
            for (int index = 0; index < Iterations; ++index)
                CodeInternal(ref v, Key, XXTEAMode.Decrypt);
            byte[] numArray = new byte[v.Length * 4];
            Buffer.BlockCopy(v, 0, numArray, 0, numArray.Length);
            InOutData = numArray;
        }

        public static byte DoEncrypt(ref byte[] InOutData)
        {
            uint[] v = new uint[(int)Math.Ceiling(InOutData.Length / 4.0)];
            Buffer.BlockCopy(InOutData, 0, v, 0, InOutData.Length);
            for (int index = 0; index < Iterations; ++index)
                CodeInternal(ref v, Key, XXTEAMode.Encrypt);
            byte[] numArray = new byte[v.Length * 4];
            Buffer.BlockCopy(v, 0, numArray, 0, numArray.Length);
            byte num = (byte)(numArray.Length - InOutData.Length);
            InOutData = numArray;
            return num;
        }

        public enum XXTEAMode
        {
            Encrypt,
            Decrypt,
        }
    }
}
