using ZCure.API.Protocol;
using ZCure.API.Models;
using ZCure.API.Requests;
using ZCure.API.Serializers;

namespace ZCure.API.Controllers
{
    public class LoadoutController : Controller
    {
        [Query(MessageType.GetLoadoutList)]
        public (bool, string) GetLoadoutList(long userId)
        {
            var loadouts = _database.Loadouts.Where(new { OwnerId = userId }).ToList();
            var data = ZCXmlSerializer.Serialize(loadouts);

            return (loadouts.Count > 0, data);
        }

        [Query(MessageType.CreateLoadout)]
        public dynamic CreateLoadout(long userId)
        {
            var user = _database.Users.Get(userId);

            if (user == null)
                return false;

            var loadout = new Loadout()
            {
                OwnerId = user.Id,
                CreatorId = user.Id,
                Installed = 1
            };

            _database.Loadouts.Insert(loadout);

            return (true, ZCXmlSerializer.Serialize(loadout));
        }

        [Query(MessageType.UpdateLoadout)]
        public dynamic UpdateLoadout(long userId, Guid loadoutId, string name, string data)
        {
            Loadout? loadout = _database.Loadouts.Get(loadoutId);

            if (loadout == null)
                return false;

            if (loadout.OwnerId != userId)
                return false;

            loadout.Name = name;
            loadout.Data = data;

            _database.Loadouts.Update(loadout);
            return (true, ZCXmlSerializer.Serialize(loadout));
        }

        [Query(MessageType.RemoveLoadout)]
        public dynamic RemoveLoadout(long userId, Guid loadoutId)
        {
            Loadout? loadout = _database.Loadouts.Get(loadoutId);

            if (loadout == null)
                return false;
            if (loadout.OwnerId != userId)
                return false;

            _database.Loadouts.Delete(loadout);

            return (true, ZCXmlSerializer.Serialize(loadout));
        }

        [Query(MessageType.ShareLoadout)]
        public (bool, string) ShareLoadout(User? user, Guid loadoutId)
        {
            Loadout? loadout = _database.Loadouts.Get(loadoutId);

            if (loadout == null)
                return (false, "");

            var sharedLoadout = loadout.Clone();
            sharedLoadout.ShareCode = Guid.NewGuid();
            _database.Loadouts.Insert(sharedLoadout);

            return (true, ZCXmlSerializer.Serialize(sharedLoadout));
        }

        [Query(MessageType.CloneLoadout)]
        public (bool, string) CloneLoadout(User? user, Guid loadoutId)
        {
            Loadout? loadout = _database.Loadouts.Get(loadoutId);

            if (loadout == null || user == null)
                return (false, "");

            if (loadout.OwnerId != user.Id)
                return (false, "");

            var newLoadout = loadout.Clone();
            _database.Loadouts.Insert(newLoadout);

            return (true, ZCXmlSerializer.Serialize(newLoadout));
        }
    }
}