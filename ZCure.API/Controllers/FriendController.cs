﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZCure.API.Models;
using ZCure.API.Protocol;
using ZCure.API.Requests;
using ZCure.API.Serializers;
using ZCure.API.Services;

namespace ZCure.API.Controllers
{
    public enum EFriendAddResponse
    {
        Success,
        UnknownPlayer,
        AlreadyPending,
        AlreadyFriend,
        AlreadyDenied,
        YourListFull,
        TheirListFull,
        FriendYourself,
        UnknownError,
    }

    public class FriendController : Controller
    {
        private FriendService _friendService => _app.GetService<FriendService>()!;

        [Query(MessageType.GetFriends)]
        public (bool, string) GetFriendlist(User? user)
        {
            if (user == null)
                return (false, "");

            var friends = _database.Friends.Where("where OwnerId = @UserId OR FriendId = @UserId", new { UserId = user.Id }).ToList();
            var data = ZCXmlSerializer.Serialize(friends);

            return (true, data);
        }

        [Query(MessageType.AddFriend)]
        public dynamic AddFriend(User? user, User? friend, string friendName, string message)
        {
            if (user == null)
                return (false, 0, EFriendAddResponse.UnknownPlayer);

            if(friend == null)
            {
                friend = _database.Users.Where(new { Name = friendName }).FirstOrDefault();
                if (friend == null)
                    return (false, user.Id, EFriendAddResponse.UnknownPlayer);
            }

            var (response, friendship) = _friendService.CreateFriendRequest(user, friend);
            return (true, user.Id, response, ZCXmlSerializer.Serialize(friendship));
        }

        [Query(MessageType.RespondFriendRequest)]
        public bool RespondFriendRequest(User? user, User? friend, bool accepted)
        {
            if (user == null || friend == null)
                return false;

            return _friendService.HandleFriendRequestResponse(user, friend, accepted);
        }

        [Query(MessageType.RemoveFriend)]
        public dynamic RemoveFriend(User? user, User? friend)
        {
            if (user == null || friend == null)
                return false;

            var myFriendship = _database.Friends.Where(new { OwnerId = user.Id, FriendId = friend.Id }).FirstOrDefault();
            var theirFriendship = _database.Friends.Where(new { OwnerId = friend.Id, FriendId = user.Id }).FirstOrDefault();

            if (myFriendship != null)
                _database.Friends.Delete(myFriendship);
            if (theirFriendship != null)
                _database.Friends.Delete(theirFriendship);

            return (true, friend.Id);
        }
    }
}
