using System.Security.Principal;
using ZCure.API.Protocol;
using ZCure.API.Models;
using ZCure.API.Requests;
using ZCure.API.Serializers;

namespace ZCure.API.Controllers
{
    public class InventoryController : Controller
    {
        [Query(MessageType.GetInventory)]
        public (bool, long, string) QueryInventory(User? user, string InvalidateCached)
        {
            if (user == null)
            {
                _log.Error($"failed to get inventory: no such user");
                return (false, 0, "");
            }

            // @todo find a performant way to create unlocked inventory item list for specific user
            var inventoryItems = _database.Inventory.GetList().ToList();
            foreach(var item in inventoryItems)
                item.Owner = user;
            
            var inventory = new Inventory(user, inventoryItems);
            var inventoryString = ZCXmlSerializer.Serialize(inventory);
            return (true, user.Id, inventoryString);
        }

        [Query(MessageType.CheckInventory)]
        public bool CheckInventory(long userId, string lastTime)
        {
            return true;
        }
    }
}