﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZCure.API.Models;
using ZCure.API.Protocol;
using ZCure.API.Requests;
using ZCure.API.Serializers;

namespace ZCure.API.Controllers
{
    public class ProgressionController : Controller
    {
        [Query(MessageType.GetUnlockLevels)]
        public (bool, string) GetUnlockLevels(long userId)
        {
            var levels = _database.Levels.GetList().ToArray();

            var response = new ZCUnlockLevelProgression()
            {
                UnlockLevels = levels
            };

            return (true, ZCXmlSerializer.Serialize(response));
        }

        [Query(MessageType.GetXP)]
        public (bool, int) GetUserExperience(User? user)
        {
            if (user == null)
                return (false, 0);

            return (true, user.Experience);
        }

        [Query(MessageType.AddXP)]
        public (bool, long) AddUserExperience(User? user, int amount)
        {
            if (user == null)
                return (false, 0);

            user.Experience += amount;
            if (_database.Users.Update(user) == 0)
            {
                _log.Error("failed to add {Amount} to user experience for {UserId}", amount, user.Id);
                return (false, user.Id);
            }

            _log.Debug("added {Amount} experience for user {UserName} ({UserId})", amount, user.Name, user.Id);
            return (true, user.Id);
        }
    }
}
