﻿using System.Security.Cryptography;
using System.Text;
using ZCure.API.Models;
using ZCure.API.Requests;
using ZCure.API.Protocol;
using NetCoreServer;
using System.Text.Json;
using Newtonsoft.Json;
using System.Net;
using ZCure.API.Caches;
using ZCure.API.Servers;
using ZCure.API.Services;
using ZCure.API.Serializers;

namespace ZCure.API.Controllers
{
    public enum EConnectionStatus : int
    {
        NotConnected,
        Connected,
        UnkownError,
        ServiceFailure,
        ServiceUnavailable,
        UpdateRequired,
        ServerMaintenance,
        DuplicateLoginDetected,
        InvalidUser,
        Banned
    }

    public enum ELoginResponse : int
    {
        OK,
        NotPending,
        Duplicate,
        Blacklisted,
        Whitelisted,
        Failed
    }

    /// <summary>
    /// Query handlers related to authentication.
    /// </summary>
    public class AuthController : Controller
    {
        private AuthCache _authCache => _app.GetCache<AuthCache>()!;
        private UserService _userService => _app.GetService<UserService>()!;
        private AuthService _authService => _app.GetService<AuthService>()!;
        private PresenceService _presenceService => _app.GetService<PresenceService>()!;

        /// <summary>
        /// Check authentication health.
        /// </summary>
        /// <param name="check"></param>
        /// <returns>authentication ready</returns>
        [Query(MessageType.Heartbeat, Auth: false)]
        public bool Heartbeat(string check) => true;

        /// <summary>
        /// User login (web server)
        /// </summary>
        /// <param name="username">name of user</param>
        /// <param name="password">passwor/steam auth ticket data</param>
        /// <param name="salt"></param>
        /// <param name="clientVersion">version of client</param>
        /// <param name="steamId">steam user id</param>
        /// <param name="language">language/region identifier of client</param>
        /// <returns>Login result containing login status, connection status, server game version, user id, authentication key</returns>
        [Query(MessageType.Login, Auth: false, Server: EServerType.Web)]
        public (bool, int, int, string, long, string) LoginWebServer(
            string username, string password, string salt, string clientVersion, long steamId, string language)
        {
            _log.Debug("user login request received for {UserName} ({SteamId}) with client version {ClientVersion}", username, steamId, clientVersion);

            var (loginResponse, connectionStatus, ticket) = _userService.SteamLogin(username, steamId, password, Session);

            long userId = 0;
            string authkey = "";

            if(ticket != null)
            {
                userId = ticket.UserId;
                authkey = ticket.AuthKey;
            }

            return (true, (int)loginResponse, (int)connectionStatus, clientVersion, userId, authkey);
        }

        [Query(MessageType.Login, Auth: true, EServerType.Presence)]
        public bool LoginNotificationServer(User? user, string userName, bool bAppearOffline)
        {
            return true;
        }

        /// <summary>
        /// User logout
        /// </summary>
        /// <param name="userId">id of user to logout</param>
        /// <returns></returns>
        [Query(MessageType.Logout)]
        public bool Logout(User? user)
        {
            _userService.Logout(user!, Session);
            return true;
        }
    }
}
