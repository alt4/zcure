using ZCure.API.Caches;
using ZCure.API.Protocol;
using ZCure.API.Requests;

namespace ZCure.API.Controllers
{
    /// <summary>
    /// File queries
    /// </summary>
    public class FileController : Controller
    {
        private FileCache _fileCache => _app.GetCache<FileCache>()!;

        /// <summary>
        /// Reads a file from cache
        /// </summary>
        /// <param name="userId">user id of query (most likely 0)</param>
        /// <param name="filename">name of file to read</param>
        /// <returns>(File read was succesfull, name of file, file content)</returns>
        [Query(MessageType.GetFile, false)]
        public (bool, string, byte[]) GetFile(long userId, string filename)
        {
            byte[]? content = _fileCache.Get(filename);

            if (content == null)
            {
                _log.Error("failed to read {FileName} from file cache", filename);
                return (false, filename, []);
            }

            return (true, filename, content);
        }

        /// <summary>
        /// Gets a list of files to read/patch for client
        /// </summary>
        /// <param name="userId">user id of query (most likely 0)</param>
        /// <returns>(file list was read, json list of files to be requested by client)</returns>
        [Query(MessageType.GetFileList, false)]
        public dynamic GetFileList(long userId)
        {
            return (true, "");
        }
    }
}