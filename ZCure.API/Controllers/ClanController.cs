using ZCure.API.Protocol;
using ZCure.API.Models.Clan;
using ZCure.API.Requests;
using ZCure.API.Serializers;

namespace ZCure.API.Controllers
{
    public class ClanController : Controller
    {
        [Query(MessageType.GetClanInfo)]
        public (bool, string) GetClanInfo(string clanTag)
        {
            var clanInfo = ZCXmlSerializer.Serialize(new ZCClanCombinedInfo());
            return (false, clanInfo);
        }

        [Query(MessageType.GetClanTag)]
        public dynamic GetClanTag(long userId)
        {
            return (true, $"{userId}:MyClan");
        }
    }
}