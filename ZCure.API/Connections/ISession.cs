using Serilog;
using System.Net;
using System.Net.Sockets;
using ZCure.API.Requests;
using ZCure.API.Servers;

namespace ZCure.API.Connections
{
    public interface ISession
    {
        public ZCureApp App { get; }
        
        public Guid Id { get; }

        public Socket Socket { get; }

        public IPAddress IpAddress { get; }
    }
}