using System.Net;
using System.Net.Sockets;
using System.Text;
using MySqlX.XDevAPI;
using NetCoreServer;
using Serilog;
using ZCure.API.Caches;
using ZCure.API.Protocol;
using ZCure.API.Requests;
using ZCure.API.Servers;

namespace ZCure.API.Connections
{
    public class HttpSession : NetCoreServer.HttpSession, ISession
    {
        public ZCureApp App { get; protected set; }
        public IPAddress IpAddress => (Socket.RemoteEndPoint as IPEndPoint)!.Address;

        private ILogger _log { get; set; }

        private Router _router => (Server as ZCure.API.Servers.HttpServer)!.Router;

        private AuthCache _authCache => App.GetCache<AuthCache>()!;

        public HttpSession(ZCure.API.Servers.HttpServer server)
            : base(server)
        {
            _log = Log.ForContext<HttpSession>();
            App = server.App;
        }

        protected override void OnConnecting()
        {
            _log = _log.ForContext("Endpoint", Socket.RemoteEndPoint);
            base.OnConnecting();
        }

        protected override void OnReceivedRequest(HttpRequest request)
        {
            _log.Verbose("received request for {Method} {Url}", request.Method, request.Url);

            if (request.Method == "GET" && request.Url == "/Status/status.txt")
            {
                SendResponseAsync(Response.MakeGetResponse("Up: All Servers Alive"));
            }
            else
            {
                try
                {
                    // get query messages from raw request
                    Message query = Message.Decrypt(request.BodyBytes);

                    // validate authentication
                    if(_router.NeedsAuthentication(query.MessageType) && !IsAuthValid(query, request))
                    {
                        _log.Error("request authentication failed for {SessionId} {MessageType}", Id, query.MessageType);
                        var errMsg = new Message(query.MessageType, 0, [(byte)'F']).Encrypt();
                        SendResponseAsync(Response.MakeErrorResponse(403, Encoding.UTF8.GetString(errMsg)));
                        return;
                    }

                    byte[] response = _router.Route(query, this);
                    SendResponseAsync(Response.MakeGetResponse(response));
                } 
                catch(Exception ex)
                {
                    _log.Error(ex, "failed to handle request {Method} {Url}", request.Method, request.Url);
                    SendResponseAsync(Response.MakeErrorResponse(500, "internal error"));
                }
            }
        }

        protected override void OnReceivedRequestError(NetCoreServer.HttpRequest request, string error)
        {
            _log.Error("http request error: {error}", error);
        }

        protected override void OnError(SocketError error)
        {
            _log.Error("http session error: {error}", error);
        }

        protected bool IsAuthValid(Message query, HttpRequest request)
        {
            for (int i = 0; i < request.Headers; i++)
            {
                var header = request.Header(i);
                if (header.Item1 == "UserID")
                {
                    long userId = long.Parse(header.Item2);

                    var ticket = _authCache.Get(userId);
                    if (ticket == null || !ticket.IsValid)
                        return false;
                    if (ticket.IP.ToString() != (Socket.RemoteEndPoint as IPEndPoint)!.Address.ToString())
                        return false;

                    break;
                }
            }

            return true;
        }
    }
}