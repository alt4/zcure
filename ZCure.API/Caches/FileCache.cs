﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;

namespace ZCure.API.Caches
{
    public class FileCache : Cache<string, byte[]>
    {        
        public string? GetString(string fileName, Encoding encoding)
        {
            byte[]? content = Get(fileName);
            
            if (content == null)
                return null;

            return encoding.GetString(content);
        }

        public string? GetString(string fileName)
            => GetString(fileName, Encoding.UTF8);

        public void Add(string fileName, string content, Encoding encoding)
            => Add(fileName, encoding.GetBytes(content));

        public void Add(string fileName, string content)
            => Add(fileName, content, Encoding.UTF8);

        public void Add(string filePath)
            => Add(Path.GetFileName(filePath), File.ReadAllBytes(filePath));

        public IEnumerable<string> GetFileList()
            => _itemCache.Keys;
    }
}
