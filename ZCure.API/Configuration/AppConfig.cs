namespace ZCure.API.Configuration
{
    public class AppConfig
    {
        public DatabaseConfig Database { get; set; } = new DatabaseConfig();
        public LoggingConfig Logging { get; set; } = new LoggingConfig();

        public SteamConfig Steam { get; set; } = new SteamConfig();

        public HttpServerConfig WebServer { get; set; } = new HttpServerConfig();
        public PresenceServerConfig PresenceServer { get; set; } = new PresenceServerConfig();

        public InventoryConfig Inventory { get; set; } = new InventoryConfig();
    }
}