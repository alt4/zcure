﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZCure.API.Configuration
{
    public class PresenceServerConfig
    {
        public int Port { get; set; } = 9004;
        public string Ip { get; set; } = "0.0.0.0";
    }
}
