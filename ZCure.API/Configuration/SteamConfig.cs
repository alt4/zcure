﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZCure.API.Configuration
{
    public class SteamConfig
    {
        public string ApiKey { get; set; } = "";
        public int AppId { get; set; } = 209870;
        public string ApiUrl { get; set; } = "api.steampowered.com";

        public bool AllowVacBan { get; set; } = false;
        public bool AllowPublisherBan { get; set; } = false;
        public bool DisableSteamAuth {get; set;} = false;
    }
}
