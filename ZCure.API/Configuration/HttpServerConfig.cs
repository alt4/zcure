namespace ZCure.API.Configuration
{
    public class HttpServerConfig
    {
        public int Port { get; set; } = 80;
        public string Ip { get; set;} = "0.0.0.0";
    }
}