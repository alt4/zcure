namespace ZCure.API.Configuration
{
    public enum DatabaseProvider
    {
        MySql,
        MsSql,
        SQLite
    }

    public class DatabaseConfig
    {
        public string Provider { get; set; } = "mysql";
        public string Host { get; set; } = "127.0.0.1";
        public int Port { get; set; } = 3306;
        public string Database { get; set; } = "zcure";
        public string User { get; set; } = "zcure";
        public string Password { get; set; } = "zcure";

        public string File { get; set; } = "";
        public string Options { get; set; } = "";

        public string ConnectionString()
        {
            switch(Provider)
            {
                case "mysql":
                    return $"Server={Host};Port={Port};Database={Database};Uid={User};Pwd={Password};{Options}";
                case "mssql":
                    return $"Server={Host};Database={Database};User Id={User};Password={Password};{Options}";
                case "sqlite":
                    return $"Data Source={File};{Options}";
                default:
                    return "";
            }
        }
    }
}