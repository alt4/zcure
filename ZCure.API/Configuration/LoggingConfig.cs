using Serilog;
using Serilog.Events;
using Serilog.Formatting.Json;

namespace ZCure.API.Configuration
{
    public enum LogTarget
    {
        Console,
        File
    }

    public class LoggingConfig
    {
        public LogTarget Target { get; set; } = LogTarget.Console;
        public string Directory { get; set; } = "";
        public LogEventLevel Level { get; set; } = LogEventLevel.Debug;

        public LoggerConfiguration GetLoggerConfig()
        {
            var config = new LoggerConfiguration()
                .MinimumLevel.Is(Level);
            
            switch(Target)
            {
                case LogTarget.Console:
                    config.WriteTo.Console(outputTemplate: "[{Timestamp:HH:mm:ss} {Level:u3}] {ClassName} {Message:lj}{NewLine}{Exception}");
                    break;
                case LogTarget.File:
                    config.WriteTo.File(new JsonFormatter(), Directory + "/log.json");
                    break;
            }

            return config;
        }
    }
}