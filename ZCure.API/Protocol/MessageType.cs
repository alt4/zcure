﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZCure.API.Protocol
{
    public enum MessageType
    {
        Invalid = -1,

        #region authentication
        Hello = 0,
        Login = 1,
        Logout = 33,
        Heartbeat = 43,
        ExternalAuth = 163,
        #endregion

        #region funds
        RemoveZP = 2,
        GetZP = 5,
        AddZP = 128,
        GetGP = 16,
        AddGP = 17,
        RemoveGP = 18,
        GetFunds = 20,
        UpdateFunds = 207,
        #endregion

        #region profile
        GetProfile = 3,
        UpdateProfile = 4,
        UpdateStats = 27,
        AddXP = 29,
        GetXP = 32,
        ChangeName = 30,
        ValidatePlayerName = 31,
        GetSkills = 39,
        UpdateSkills = 40,
        ToggleProfileFlag = 60,
        CreateCharacter = 84,
        ResetProfile = 87,
        GetUserIdByName = 103,
        GetUserStats = 122,
        UpdateUserStats = 143,
        RegisterUser = 150,
        WriteStatRule = 151,
        GetStatRule = 152,
        ResetStat = 153,
        RemoveStatRule = 154,
        GetProfileByName = 160,
        GetProfileBatch = 161,
        GetProfileBatchByName = 162,
        UnlockProfileFlag = 170,
        GetUserSettings = 176,
        UpdateUserSettings = 177,
        DeleteProfile = 182,
        CreateProfile = 183,
        GetProfileFlags = 184,
        ProfileFlagToggleHelper = 185,
        #endregion

        #region inventory
        GetInventory = 6,
        CreateInventory = 7,
        RemoveInventory = 14,
        UpdateInventory = 16,
        AddInventoryItem = 13,
        ActivateInventoryItem = 28,
        InventoryUpdated = 51,
        RenewInventory = 53,
        DeleteInventory = 54,
        FuseInventory = 97,
        FuseDatanodes = 98,
        GetInventoryItem = 135,
        ConsumeItem = 141,
        MarkInventoryItemSeen = 155,
        GetPermanentItems = 157,
        AddPermanentItem = 158,
        MarkPermanentItemSeen = 167,
        WipeInventory = 198,
        DeletePermanentItem = 201,
        #endregion

        #region store
        GetUserstore = 9,
        CreateStoreItem = 10,
        RemoveStoreItem = 11,
        UpdateStoreItem = 12,
        PurchaseItem = 19,
        GetOffers = 46,
        CreateOffer = 47,
        UpdateOffer = 48,
        DeleteOffer = 49,
        GiftStoreItem = 66,
        GiftOffer = 105,
        GetUnlockLevels = 156,
        RedeemCode = 159,
        GetOfferFilters = 178,
        CreateOfferFilter = 179,
        UpdateOfferFilter = 180,
        DeleteOfferFilter = 181,
        RefundPurchase = 202,
        RedeemCode2 = 204,
        CheckCode = 205,
        ConsumeCode = 206,
        UpdateRedeemCodeHistory = 206,
        #endregion

        #region  mails
        CreateMail = 68,
        DeleteMail = 69,
        ReceiveMailItems = 70,
        GetMails = 71,
        MarkMailRead = 72,
        NotifyNewMail = 80,
        #endregion

        #region server
        CheckServerAvailability = 8,
        GetServers = 21,
        GetServerDetails = 22,
        RegisterServer = 24,
        UnregisterServer = 25,
        UpdateServer = 26,
        RegisterMasterServer = 34,
        UnregisterMasterServer = 35,
        UpdateMasterServer = 36,
        IncreaseMasterServerCapacity = 88,
        CreateGame = 37,
        CreateGameCompleted = 38,
        CheckDatabaseAvailability = 44,
        GetPlayerList = 61,
        JoinRankedGame = 73,
        LeaveRankedGame = 74,
        RankedGameStarted = 75,
        ReportRankedGameStat = 76,
        GetLeaderboard = 77,
        GetTopLeaderboard = 78,
        KickPlayer = 85,
        CheckServerHealth = 90,
        CheckLoginQueue = 104,
        SetGameRules = 119,
        GetPremiumServerList = 136,
        UpdatePremiumServer = 137,
        ShutdownServer = 144,
        UpdateServerRule = 146,
        DeletePrivateServer = 148,
        RegisterServerFailed = 149,
        GetServerStats = 168,
        GetWebServerStats = 169,
        GetQualityOfService = 171,
        #endregion

        #region friends
        GetFriendList = 23,
        GetFriends = 91,
        AddFriend = 92,
        RemoveFriend = 93,
        RespondFriendRequest,
        UpdateFriend = 95,
        FriendUpdated = 210,
        FriendAdded = 212,
        #endregion

        #region chat
        Chat = 41,
        GetVoice = 52,
        DirectMessage = 67,
        GetChatList = 79,
        LeaveChatRoom = 108,
        UpdateChatRoom = 109,
        KickFromChat = 110,
        PromoteChat = 111,
        DeleteChatRoom = 112,
        JoinChat = 139,
        LeaveChat = 140,
        SearchClan = 142,
        LeaveAllChatRooms = 187,
        #endregion

        #region clan
        ValidateClanInfo = 42,
        ChangeClanLeader = 45,
        MOTD = 55,
        CreateClan = 56,
        DisbandClan = 57,
        InviteToClan = 58,
        InviteToClanResponse = 59,
        UpdateClanMembers = 63,
        GetClanInfo = 64,
        GetClanInfo2 = 65,
        GetClanMembers = 81,
        GetClanTag = 116,
        JoinClan = 117,
        LeaveClan = 118,
        KickClanMember = 123,
        KickClanMemberResponse = 124,
        ChangeClanRank = 131,
        ExpandClan = 213,
        ClanUpdated = 214,
        #endregion

        #region files
        GetFile = 89,
        UploadFile = 125,
        GetFileList = 221,
        #endregion

        #region match finding
        SearchForPlayers = 99,
        StartMatch = 101,
        EndMatch = 102,
        InviteToParty = 106,
        RespondPartyInvite = 107,
        SearchPartyGame = 114,
        JoinPartyGame = 115,
        CreatePrivateMatch = 126,
        SendGameInvite = 129,
        ReceiveGameInvite = 130,
        SendPublicGameInvite = 133,
        ReceivePublicGameInvite = 134,
        ReceivePartyInvite = 138,
        #endregion

        #region presence
        UpdatePresence = 121,
        SubscribePresence = 188,
        UnsubscribePresence = 189,
        SubscribeGroupPresence = 190,
        UnsubscribeGroupPresence = 191,
        PresenceUpdated = 211,
        #endregion

        #region missions
        GetMissions = 145,
        RedeemMission = 147,
        MissionUpdated = 172,
        UpdateMission = 173,
        AddMission = 174,
        #endregion

        #region loadouts
        GetLoadoutList = 222,
        CreateLoadout = 223,
        UpdateLoadout = 224,
        RemoveLoadout = 225,
        ShareLoadout = 227,
        CheckInventory = 230,
        CloneLoadout = 231,
        #endregion

        RemoteCommand = 50,
        AddEvent = 62,
        GetEvents = 82,
        NotifyAdminMessage = 83,
        LogEvent = 86,
        LogGame = 100,
        InvalidateCache = 120,
        RouteMessage = 127,
        GetIpLocation = 132,
        WriteUserCache = 164,
        GetUserCache = 165,
        ClearUserCache = 166,
        ClearCache = 175,
        GetServices = 186,
        GetEventByUser = 199,
        GetEventsByTypes = 200,

        AddTransaction = 192,
        AddBatchTransaction = 193,
        GetTransactions = 194,
        GetTransactionById = 195,
        GetTransactionByTargetId = 196,
        RemoveTransactionOfUser = 197,
        GetCustomTransaction = 208,
        GetTransactionByTargetAndType = 209,

        GetServerId = 251,
        GetMatchId = 252,
        SetConfig = 253,
        CreateCrashReport = 254,
        WebWrapper = 256,
        DatabaseRequest = 257,
        ObjectWrapper = 258,
    }
}
